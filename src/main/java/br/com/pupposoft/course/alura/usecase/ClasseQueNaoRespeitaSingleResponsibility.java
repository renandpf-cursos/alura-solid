package br.com.pupposoft.course.alura.usecase;

import java.math.BigDecimal;

import br.com.pupposoft.course.alura.domain.Funcionario;
import br.com.pupposoft.course.alura.domain.Salario;
import br.com.pupposoft.course.alura.gateway.AnyGateway;


/*
 * 
 * "S" -> Single Responsibility: "Uma classe deveria ter um unico motivo para mudar"
 * 	fere pois ela faz muitas coisas (métodos).
 *  'Single Responsibility' esta relacionado a coesão
 * 	Isso afeta a coesão, pois  a classe cresce demais etc
 * 	Por exemplo, se vc precisar mudar o reajuste será nessa classe. E o frete também! Ou seja: 
 * 	ela tem mais de 1 motivo pra mudar. 
 * 
 * 	O ideal é a classe ter um unico motivo. No caso, se precisar mudar a regra de reajuste 
 * 	o correto é mudar a classe Salario. Ou se mudar alguma regra de calculo de idade, deve mudar em Pessoa.
 * 	
 * 
 */
public class ClasseQueNaoRespeitaSingleResponsibility {

	private AnyGateway gateway;
	
	public void anyAction(final long id) {
		final Funcionario funcionario = gateway.getById(id);
		final BigDecimal reajuste = funcionario.getValorTotalReajustes();
		//TODO: implementar...
	}
	
	
	//ERRADOOO!
	//Se mudar a regra de reajuste.. deu ruim!
	public void calcularReajuste(final long id) {
		final Funcionario funcionario = gateway.getById(id);
		final Salario salario = funcionario.getSalario();
		final BigDecimal reajuste = funcionario.getReajustes();
		
		//TODO: implementar...
	}
	
	//ERRADOOO!
	//Se mudar a regra de reajuste.. deu ruim!
	public void calcularFrete(final long id) {
		final Funcionario funcionario = gateway.getById(id);
		final Salario salario = funcionario.getSalario();
		final BigDecimal reajuste = funcionario.getReajustes();
		
		//TODO: implementar...
	}
	
}
