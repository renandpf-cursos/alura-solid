package br.com.pupposoft.course.alura.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

/*
 * Principio de substituição de Liskov:
 * Sugere cuidado na herança: No caso, os dados de funcionario são os mesmos de tercerizados, porém  
 *  se Tercerizado herdar de Funcionario, um tercerizado terá também as mesmas operações (métodos) de funcionário.
 *  Então, um tercerizado teria ações que NÃO deveria ter, do tipo: 'getValorTotalReajustes', 'promocaoCargo', etc
 *  
 *  A solução neste caso, seria usar 'composição', extraindo os atributos comuns em 1 classe 'DadosPessoais'
 *  E a herança deixa de existir.
 *   
 * 
 */
@AllArgsConstructor
@Getter
public class Tercerizado /*extends Funcionario*/ { //Maneira errada (herança ruim) !
	private DadosPessoais dadosPessoais;
}
