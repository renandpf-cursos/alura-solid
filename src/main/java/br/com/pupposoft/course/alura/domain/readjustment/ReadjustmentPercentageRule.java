package br.com.pupposoft.course.alura.domain.readjustment;

import java.math.BigDecimal;
import java.math.RoundingMode;

import br.com.pupposoft.course.alura.domain.Salario;
import br.com.pupposoft.course.alura.usecase.exception.SalaryReadjustmentGreaterAllowed;

public class ReadjustmentPercentageRule implements SalaryReadjustmentRule {

	public void validate(Salario salary, BigDecimal aumento) {
		BigDecimal percentualReajuste = aumento.divide(salary.getValor(), RoundingMode.HALF_UP);
		if(percentualReajuste.compareTo(new BigDecimal("0.4")) > 0){
			throw new SalaryReadjustmentGreaterAllowed();
		}
	}
	
}
