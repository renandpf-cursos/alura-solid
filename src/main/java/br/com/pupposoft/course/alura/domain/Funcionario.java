package br.com.pupposoft.course.alura.domain;

import java.math.BigDecimal;

import lombok.Getter;


/**
 * 
 * Acoplamento: "Sempre" haverá acoplamento entre classe. Mas isso deve ser controlado.
 * Cada classe deve responder pelos seus dados
 * 
 * @author Renan
  */
@Getter
public class Funcionario {

	
	//Propriedades trasferidas para DadosPessoais para respeitar o principio de Liskov
//	private String nome;
//	private String cpf;
	
	private DadosPessoais dadosPessoais;
	private Cargo cargo;
	private Salario salario;
	
	public boolean isGerente() {
		return Cargo.GERENTE.equals(cargo);
	}

	//CORRETO!!  (Junção de encapsulamento + baixo acoplamento)
	public BigDecimal getValorTotalReajustes() {
		//TODO: implementar
		return new BigDecimal("0.000");
	}


	public BigDecimal getReajustes() {
		// TODO Implementar
		return null;
	}
	
	public void promover() {
		if(isGerente()) {
			throw new RuntimeException("Gerentes não pode ser promovido!");//NOSONAR
		}
		
		cargo = cargo.getProximoCargo();
	}
	
}

