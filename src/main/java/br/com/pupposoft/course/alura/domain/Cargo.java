package br.com.pupposoft.course.alura.domain;

public enum Cargo {
	FUNCIONARIO {
		@Override
		public Cargo getProximoCargo() {
			return GERENTE;
		}
	},
	
	GERENTE {
		@Override
		public Cargo getProximoCargo() {
			return GERENTE;
		}
	};
	
	public abstract Cargo getProximoCargo();
	
}
