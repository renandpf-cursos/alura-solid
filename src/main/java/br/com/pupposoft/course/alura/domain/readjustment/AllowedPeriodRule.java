package br.com.pupposoft.course.alura.domain.readjustment;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import br.com.pupposoft.course.alura.domain.Salario;
import br.com.pupposoft.course.alura.usecase.exception.InvalidPeriodSalaryReadjustment;

public class AllowedPeriodRule implements SalaryReadjustmentRule {

	public void validate(Salario salary, BigDecimal aumento) {
		LocalDate now = LocalDate.now();
		long amountMonthsSalaryReadjustment = ChronoUnit.MONTHS.between(salary.getDataReajuste(), now);
		if(amountMonthsSalaryReadjustment > 6) {
			throw new InvalidPeriodSalaryReadjustment();
		}

	}
	
}
