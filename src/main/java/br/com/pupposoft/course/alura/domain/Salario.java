package br.com.pupposoft.course.alura.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import br.com.pupposoft.course.alura.domain.readjustment.SalaryReadjustmentRule;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Salario {
	private BigDecimal valor;
	private LocalDate dataReajuste;
	private List<SalaryReadjustmentRule> salaryReadjustmentRules;

	/*
	 * Não feriu o principio 'O' -> "Open Closed Principle": 
	 * 	"Entidades de software (classes, módulos, funções, etc) devem estar abertas para extensão, porém fechada para modificação"
	 * 	- Significa que: numa nova feature (extensão) o ideal é poder adicionar novos códigos, sem precisar modificar código ja existente.
	 * 		(na medida do possível).
	 * 	Claro que se é uma nova funcionalidade o código será modificado, mas sem precisar alterar demais códigos
	 * 
	 * - Utilizar este principio, facilita em deixar o código mais extensível
	 * 
	 * 
	 */
	public void reajustarSalario(BigDecimal aumento) {
		salaryReadjustmentRules.forEach(rule -> rule.validate(this, aumento));
		valor = valor.add(aumento); 
		dataReajuste = LocalDate.now();
	}

	/*
	 * Feriu o principio 'O' -> "Open Closed Principle" 
	 * - Esta classe pode crescer muito.
	 * - Sempre q surgir uma nova regra ou alterar uma regra precisa mexer neste método
	 * 
	 * 
	 */
//	public void reajustarSalario(BigDecimal aumento) {
//		//REGRA 1 - percentual de ajustes
//		BigDecimal percentualReajuste = aumento.divide(valor, RoundingMode.HALF_UP);
//		if(percentualReajuste.compareTo(new BigDecimal("0.4")) > 0){
//			throw new SalaryReadjustmentGreaterAllowed();
//		}
//		
//		//REGRA 2 - periodo percentual de ajustes
//		LocalDate now = LocalDate.now();
//		long amountMonthsSalaryReadjustment = ChronoUnit.MONTHS.between(dataReajuste, now);
//		if(amountMonthsSalaryReadjustment > 6) {
//			throw new InvalidPeriodSalaryReadjustment();
//		}
//		
//		valor = valor.add(aumento); 
//		dataReajuste = LocalDate.now();
//	}

}
