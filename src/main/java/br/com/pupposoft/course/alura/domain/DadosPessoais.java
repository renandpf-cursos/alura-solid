package br.com.pupposoft.course.alura.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class DadosPessoais {
	private String nome;
	private String cpf;
}
