package br.com.pupposoft.course.alura.domain.readjustment;

import java.math.BigDecimal;

import br.com.pupposoft.course.alura.domain.Salario;

public interface SalaryReadjustmentRule {

	public void validate(Salario salary, BigDecimal aumento);
	
}
