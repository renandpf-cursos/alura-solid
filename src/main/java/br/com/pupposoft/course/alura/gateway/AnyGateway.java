package br.com.pupposoft.course.alura.gateway;

import br.com.pupposoft.course.alura.domain.Funcionario;

public interface AnyGateway {

	Funcionario getById(long id);

}
